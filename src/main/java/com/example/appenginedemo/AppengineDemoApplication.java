package com.example.appenginedemo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AppengineDemoApplication {

    @Value("${app.thing.name:no thing}")
    String thingName;

	public static void main(String[] args) {
		SpringApplication.run(AppengineDemoApplication.class, args);
	}

	/*
	@Bean
	CommandLineRunner commandLineRunner(
			ThingRepository repository
	) {
		return args -> {
			repository.save(new Thing(thingName));
		};
	}*/

}
