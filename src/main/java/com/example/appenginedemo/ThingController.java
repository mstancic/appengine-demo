package com.example.appenginedemo;

import org.springframework.beans.factory.annotation.Value;
//import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/things")
public class ThingController {

    @Value("${app.thing.name:no thing}")
    String thingName;

    private ThingRepository repository;

//    public ThingController(ThingRepository repository) {
//        this.repository = repository;
//    }


    public ThingController() {
    }

    @GetMapping
    public List<Thing> listThings() {
//        return repository.findAll();
        return Arrays.asList(new Thing[]{new Thing(thingName)});
    }
}
